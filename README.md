# README #

Author: Michael Schmutzer (michael.schmutzer12@ic.ac.uk)

### Modelling ecological divergence in bacteria ###

This repository contains code written in partial fulfilment of the requirements for the MSc in Computational Methods in Ecology and Evolution, Imperial College London. The project investigated the role of homologous recombination and gene content variation in the ecological divergence of bacteria.

This repository contains code to generate and analyse simulations of a diverging bacterial population. 
CONTENTS
-------------------
./Code/Bacteria_cluster.R           # Simulation code
./Code/Cluster_analysis.R           # Analysis code
